/**
 * Main application logic.
 *
 * @module index
 */

'use strict';

const IMAGE_H = 256;
const IMAGE_W = 256;

const { createCanvas, Image } = require('canvas');
const tf                      = require('@tensorflow/tfjs');
const loader                  = require('lib/loader');
// TODO: keep either loader or data
const data                    = require('lib/data');

const canvas = createCanvas(IMAGE_H, IMAGE_W);
const ctx = canvas.getContext('2d');
const series = loader.loadSeries();

// output some header info
console.log("Number of images read is " + series.images.length);
console.log("Each slice is " + series.images[0].getCols() + " x " + series.images[0].getRows());
console.log("Each voxel is " + series.images[0].getBitsAllocated() + " bits, " +
    (series.images[0].littleEndian ? "little" : "big") + " endian");

/**
 * TF starts here.
 */

// TODO: iterate through the array of images

const img = new Image(series.images[0]);

img.onload = () => {
    ctx.drawImage(img, 0, 0);
};

tf.tidy(() => {
    const imgTensor = tf.fromPixels(canvas).toFloat();

    console.log(imgTensor);
    // TODO:
});

/**
 * Creates a convolutional neural network (Convnet) for the MNIST data.
 *
 * @returns {tf.Model} An instance of tf.Model.
 */
function createConvModel() {
    const model = tf.sequential();

    model.add(tf.layers.conv2d({
        inputShape: [IMAGE_H, IMAGE_W, 1],
        kernelSize: 3,
        filters: 16,
        activation: 'relu'
    }));

    model.add(tf.layers.maxPooling2d({poolSize: 2, strides: 2}));
    model.add(tf.layers.conv2d({kernelSize: 3, filters: 32, activation: 'relu'}));
    model.add(tf.layers.maxPooling2d({poolSize: 2, strides: 2}));
    model.add(tf.layers.conv2d({kernelSize: 3, filters: 32, activation: 'relu'}));
    model.add(tf.layers.flatten({}));
    model.add(tf.layers.dense({units: 64, activation: 'relu'}));
    model.add(tf.layers.dense({units: 10, activation: 'softmax'}));

    return model;
}

/**
 * Compile and train the given model.
 *
 * @param {*} model The model to
 */
async function train(model) {
    const LEARNING_RATE = 0.01;
    const optimizer = 'rmsprop';

    model.compile({
        optimizer,
        loss: 'categoricalCrossentropy',
        metrics: ['accuracy'],
    });

    const batchSize = 64;

    const validationSplit = 0.15;

    // TODO: labels
    // TODO: data for training
    // TODO: data for testing

    let trainBatchCount = 0;

    const trainData = data.getTrainData();
    const testData = data.getTestData();

    const totalNumBatches =
            Math.ceil(trainData.xs.shape[0] * (1 - validationSplit) / batchSize) *
            trainEpochs;

    let valAcc;
    await model.fit(trainData.xs, trainData.labels, {
        batchSize,
        validationSplit,
        epochs: trainEpochs,
        callbacks: {
            onBatchEnd: async (batch, logs) => {
                trainBatchCount++;
                console.log(
                    `Training... (` +
                    `${(trainBatchCount / totalNumBatches * 100).toFixed(1)}%` +
                    ` complete). To stop training, refresh or close page.`);
                // ui.plotLoss(trainBatchCount, logs.loss, 'train');
                // ui.plotAccuracy(trainBatchCount, logs.acc, 'train');
                await tf.nextFrame();
            },
            onEpochEnd: async (epoch, logs) => {
                valAcc = logs.val_acc;
                // ui.plotLoss(trainBatchCount, logs.val_loss, 'validation');
                // ui.plotAccuracy(trainBatchCount, logs.val_acc, 'validation');
                await tf.nextFrame();
            }
        }
    });

    const testResult = model.evaluate(testData.xs, testData.labels);
    const testAccPercent = testResult[1].dataSync()[0] * 100;
    const finalValAccPercent = valAcc * 100;
    console.log(
        `Final validation accuracy: ${finalValAccPercent.toFixed(1)}%; ` +
        `Final test accuracy: ${testAccPercent.toFixed(1)}%`);
}

/**
 * Show predictions on a number of test examples.
 *
 * @param {tf.Model} model The model to be used for making the predictions.
 */
async function showPredictions(model) {
    const testExamples = 100;
    const examples = data.getTestData(testExamples);

    tf.tidy(() => {
        const output = model.predict(examples.xs);

        const axis = 1;
        const labels = Array.from(examples.labels.argMax(axis).dataSync());
        const predictions = Array.from(output.argMax(axis).dataSync());

        // show prediction result
        console.log(examples, predictions, labels);
    });
}

/**
 * Dataset.
 *
 * @type {CTData}
 */
let data;
async function load() {
    data = new CTData();
    await data.load();
}

/**
 * Main function.
 */
(async function () {
    console.log('Loading CT scans...');
    await data.load();

    console.log('Creating convolutional model...');
    const model = createConvModel();
    model.summary();

    console.log('Training model...');
    await train(model);

    // showPredictions(model);
})();
