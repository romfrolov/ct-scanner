/**
 * Load serise of data.
 *
 * @module lib/loader
 */

'use strict';

const PATH = '../Dataset/386348';

const fs            = require('fs');
const daikon        = require('daikon');
const toArrayBuffer = require('to-arraybuffer');

const series = new daikon.Series();
const files = fs.readdirSync(PATH);

exports.loadSeries = function loadSeries() {
    // load images
    for (let ctr in files) {
        const name = PATH + '/' + files[ctr];
        const buf = fs.readFileSync(name);

        // parse DICOM file
        const image = daikon.Series.parseImage(new DataView(toArrayBuffer(buf)));

        if (image === null) {
            console.error(daikon.Series.parserError);
        } else if (image.hasPixelData()) {
            // if it's part of the same series, add it
            if ((series.images.length === 0) ||
                    (image.getSeriesId() === series.images[0].getSeriesId())) {
                series.addImage(image);
            }
        }
    }

    // order the image files, determines number of frames, etc.
    series.buildSeries();

    return series;
};
